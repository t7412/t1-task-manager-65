package ru.t1.chubarov.tm.service;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.chubarov.tm.api.IPropertyService;

@Getter
@Service
@PropertySource("classpath:application.properties")
public final class PropertyService implements IPropertyService {

    @Value("#{environment['database.username']}")
    private String databaseUsername;

    @Value("#{environment['database.password']}")
    private String databasePassword;

    @Value("#{environment['database.url']}")
    private String databaseUrl;

    @Value("#{environment['database.driver']}")
    private String databaseDriver;

    @Value("#{environment['database.dialect']}")
    private String databaseDialect;

    @Value("#{environment['database.hbm2ddl_auto']}")
    private String databaseHbm2DDL_Auto;

    @Value("#{environment['database.show_sql']}")
    private String databaseShowSql;

//    @Value("#{environment['database.cache.use_second_level_cache']}")
//    private String databaseUseSecond;
//
//    @Value("#{environment['database.cache.use_query_cache']}")
//    private String databaseUseQueryCache;
//
//    @Value("#{environment['database.cache.use_minimal_puts']}")
//    private String databaseMinimalPuts;
//
//    @Value("#{environment['database.cache.region_prefix']}")
//    private String databaseRegionPrefix;
//
//    @Value("#{environment['database.cache.region.factory_class']}")
//    private String databaseRegionFactory;
//
//    @Value("#{environment['database.cache.provider_configuration_file_resource_path']}")
//    private String databaseProviderConfig;

}
