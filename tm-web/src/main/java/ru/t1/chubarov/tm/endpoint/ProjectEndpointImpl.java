package ru.t1.chubarov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.chubarov.tm.api.IProjectEndpoint;
import ru.t1.chubarov.tm.model.Project;
import ru.t1.chubarov.tm.service.ProjectService;

import java.util.Collection;

@RestController
@RequestMapping("/api/projects")
public class ProjectEndpointImpl implements IProjectEndpoint {

    @Autowired
    private ProjectService projectService;

    @Override
    @GetMapping("/findAll")
    public Collection<Project> findAll() {
        return projectService.findAll();
    }

    @PostMapping("/create")
    public Project create() {
        return projectService.create();
    }

    @PostMapping("/add")
    public Project add(@RequestBody final Project project) {
        return projectService.add(project);
    }

    @Override
    @PostMapping("/save")
    public Project save(@RequestBody final Project project) {
        return projectService.save(project);
    }

    @Override
    @GetMapping("/findById/{id}")
    public Project findById(@PathVariable("id") final String id) {
        return projectService.findById(id);
    }

    @Override
    @PostMapping("/delete/{id}")
    public void delete(@PathVariable("id") final String id) {
        projectService.removeById(id);
    }

}
