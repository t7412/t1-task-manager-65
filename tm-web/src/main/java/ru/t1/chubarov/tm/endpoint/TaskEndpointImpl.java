package ru.t1.chubarov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.chubarov.tm.api.ITaskEndpoint;
import ru.t1.chubarov.tm.model.Task;
import ru.t1.chubarov.tm.service.TaskService;

import java.util.Collection;

@RestController
@RequestMapping("/api/tasks")
public class TaskEndpointImpl implements ITaskEndpoint {

    @Autowired
    TaskService taskService;

    @PostMapping("/create")
    public Task create() {
        return taskService.create("NewTask", "NewDesc");
    }

    @PostMapping("/add")
    public Task add(@RequestBody final Task task) {
        return taskService.add(task);
    }

    @Override
    @GetMapping("/findAll")
    public Collection<Task> findAll() {
        return taskService.findAll();
    }

    @Override
    @PostMapping("/save")
    public Task save(@RequestBody final Task task) {
        return taskService.save(task);
    }

    @Override
    @GetMapping("/findById/{id}")
    public Task findById(@PathVariable("id") final String id) {
        return taskService.findById(id);
    }

    @Override
    @PostMapping("/delete/{id}")
    public void delete(@PathVariable("id") final String id) {
        taskService.removeById(id);
    }

}
