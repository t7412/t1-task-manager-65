package ru.t1.chubarov.tm.client;

import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.t1.chubarov.tm.model.Task;

import java.util.Collection;

public interface TaskRestEndpointClient {

    String BASE_URL = "http://localhost:8080/api/tasks";

    static TaskRestEndpointClient client() {
        @NotNull final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        @NotNull final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(TaskRestEndpointClient.class, BASE_URL);
    }

    @PostMapping("/create")
    Task create();

    @PostMapping("/add")
    Task add(@RequestBody final Task task);

    @GetMapping("/findAll")
    Collection<Task> findAll();

    @PostMapping("/save")
    Task save(@RequestBody final Task task);

    @GetMapping("/findById/{id}")
    Task findById(@PathVariable("id") final String id);

    @PostMapping("/delete/{id}")
    void delete(@PathVariable("id") final String id);

}
