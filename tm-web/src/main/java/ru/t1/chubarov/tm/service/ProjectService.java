package ru.t1.chubarov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.chubarov.tm.api.IProjectRepository;
import ru.t1.chubarov.tm.model.Project;
import javax.persistence.EntityNotFoundException;
import java.util.List;


@Service
@NoArgsConstructor
public class ProjectService {

    @NotNull
    @Autowired
    private IProjectRepository repository;

    @NotNull
    @Transactional
    public Project create() {
        @Nullable Project project = new Project("new","new_desc");
        repository.save(project);
        return project;
    }

    @Transactional
    public Project add(@NotNull final Project project) {
        if (project.getName().isEmpty()) throw new EntityNotFoundException();
        repository.save(project);
        return repository.findById(project.getId()).orElse(null);
    }

    @Transactional
    public Project save(@NotNull final Project project) {
        if (project.getName().isEmpty()) throw new EntityNotFoundException();
        repository.save(project);
        return project;
    }

    public List<Project> findAll() {
        return repository.findAll();
    }

    public Project findById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EntityNotFoundException();
        return repository.findById(id).orElse(null);
    }

    @Transactional
    public void removeById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EntityNotFoundException();
        Project find_project = repository.findById(id).orElse(null);
        if (find_project != null)  repository.deleteById(id);
    }

    @Transactional
    public void clear() {
        repository.deleteAll();
    }

    public Project findOneById(@Nullable String id) {
        return repository.findById(id).orElse(null);
    }

}
