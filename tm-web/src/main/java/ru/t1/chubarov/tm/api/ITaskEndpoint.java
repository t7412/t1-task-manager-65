package ru.t1.chubarov.tm.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.t1.chubarov.tm.model.Task;

import java.util.Collection;

public interface ITaskEndpoint {

    @GetMapping("/findAll")
    Collection<Task> findAll();

    @PostMapping("/save")
    Task save(@RequestBody Task task);

    @GetMapping("/findById/{id}")
    Task findById(@PathVariable("id") String id);

    @GetMapping("/delete/{id}")
    void delete(@PathVariable("id") String id);

}
