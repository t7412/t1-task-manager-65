package ru.t1.chubarov.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.chubarov.tm.client.ProjectRestEndpointClient;
import ru.t1.chubarov.tm.marker.IntegrationCategory;
import ru.t1.chubarov.tm.model.Project;

import java.util.Collection;

@Category(IntegrationCategory.class)
public class ProjectRestEndpointTest {

    @NotNull
    final ProjectRestEndpointClient client = ProjectRestEndpointClient.client();

    @NotNull
    final Project project_1 = new Project("TestProject_1","Desc_Project_1");

    @NotNull
    final Project project_2 = new Project("TestProject_2","Desc_Project_2");

    @NotNull
    final Project project_3 = new Project("TestProject_3","Desc_Project_3");

    @Before
    public void initTest() {
        client.add(project_1);
        client.add(project_2);
    }

    @After
    public void finish() {
        client.delete(project_1.getId());
        client.delete(project_2.getId());
        client.delete(project_3.getId());
    }

    @Test
    public void testFindAll() {
        @Nullable final Collection<Project> projects = client.findAll();
        Assert.assertEquals(projects.size(), 2);
    }

    @Test
    public void testCreate() {
        @Nullable final Project project = client.create();
        Assert.assertNotNull(project);
        client.delete(project.getId());
    }

    @Test
    public void testAdd() {
        @Nullable final Project project = client.add(project_3);
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getName(), "TestProject_3");
    }

    @Test
    public void testFindById() {
        @Nullable final Project project = client.findById(project_1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getId(), project_1.getId());
    }

    @Test
    public void testDelete() {
        client.delete(project_2.getId());
        Assert.assertNull(client.findById(project_2.getId()));
    }

    @Test
    public void testSave() {
        @Nullable final Project project = new Project("Test_Save+Project","Desc_Project_Test");
        client.save(project);
        @Nullable final Project project_check = client.findById(project.getId());
        Assert.assertNotNull(project_check);
        Assert.assertEquals(project_check.getId(), project.getId());
        Assert.assertEquals(project_check.getName(), "Test_Save+Project");
        client.delete(project.getId());
    }

}
