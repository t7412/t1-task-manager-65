package ru.t1.chubarov.tm.service;


import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.jms.*;

@Service
@NoArgsConstructor
public class ReceiverService implements ru.t1.chubarov.tm.api.IReceiverService {

    @NotNull
    @Autowired
    private ConnectionFactory connectionFactory;

    private static final String QUEUE = "LOGGER";

    @Override
    @SneakyThrows
    public void receive(@NotNull @Autowired MessageListener listener) {
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Destination destination = session.createTopic(QUEUE);
        @NotNull final MessageConsumer messageConsumer = session.createConsumer(destination);
        messageConsumer.setMessageListener(listener);
    }

}
