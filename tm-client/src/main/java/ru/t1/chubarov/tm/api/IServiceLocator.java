package ru.t1.chubarov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.endpoint.*;
import ru.t1.chubarov.tm.api.service.ICommandService;
import ru.t1.chubarov.tm.api.service.ILoggerService;
import ru.t1.chubarov.tm.api.service.ITokenService;

public interface IServiceLocator {

    @Nullable
    ICommandService getCommandService();

    @NotNull
    ITokenService getTokenService();

    @Nullable
    IDomainEndpoint getDomainEndpoint();

    @Nullable
    IProjectEndpoint getProjectEndpoint();

    @Nullable
    ITaskEndpoint getTaskEndpoint();

    @Nullable
    IAuthEndpoint getAuthEndpoint();

    @Nullable
    IUserEndpoint getUserEndpoint();

    @Nullable
    ISystemEndpoint getSystemEndpoint();

}
