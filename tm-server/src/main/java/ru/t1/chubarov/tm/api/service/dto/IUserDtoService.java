package ru.t1.chubarov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.dto.model.UserDTO;

import java.util.Collection;
import java.util.List;

public interface IUserDtoService {

    @NotNull
    UserDTO create(@NotNull String login, @NotNull String password) throws Exception;

    @NotNull
    UserDTO create(@NotNull String login, @NotNull String password, @NotNull String email) throws Exception;

    @NotNull
    UserDTO create(@NotNull String login, @NotNull String password, @NotNull Role role) throws Exception;

    boolean isLoginExist(@Nullable String login) throws Exception;

    boolean isEmailExist(@Nullable String email) throws Exception;

    @Nullable
    UserDTO findByLogin(@NotNull String login) throws Exception;

    @Nullable
    UserDTO findByEmail(@NotNull String email) throws Exception;

    @NotNull
    UserDTO findOneById(@Nullable String id) throws Exception;

    @Nullable
    UserDTO removeOne(@NotNull UserDTO model) throws Exception;

    @Nullable
    UserDTO removeByLogin(@NotNull String login) throws Exception;

    @NotNull
    UserDTO removeByEmail(@NotNull String email) throws Exception;

    @Nullable
    UserDTO setPassword(@Nullable String id, @Nullable String password) throws Exception;

    @Nullable
    UserDTO updateUser(@Nullable String id,
                       @Nullable String firstName,
                       @Nullable String lastName,
                       @Nullable String middleName) throws Exception;

    void lockUserByLogin(@Nullable String login) throws Exception;

    void unlockUserByLogin(@Nullable String login) throws Exception;

    @NotNull
    Collection<UserDTO> set(@NotNull Collection<UserDTO> models) throws Exception;

    void clear() throws Exception;

    @NotNull
    List<UserDTO> findAll() throws Exception;

    long getSize() throws Exception;

}
