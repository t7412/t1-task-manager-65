package ru.t1.chubarov.tm.service.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.chubarov.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.chubarov.tm.api.service.IPropertyService;
import ru.t1.chubarov.tm.api.service.dto.IProjectDtoService;
import ru.t1.chubarov.tm.api.service.dto.ITaskDtoService;
import ru.t1.chubarov.tm.api.service.dto.IUserDtoService;
import ru.t1.chubarov.tm.dto.model.UserDTO;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.chubarov.tm.exception.field.*;
import ru.t1.chubarov.tm.exception.user.UserNotFoundException;
import ru.t1.chubarov.tm.util.HashUtil;

import java.util.Collection;
import java.util.List;

@Getter
@Service
@NoArgsConstructor
@AllArgsConstructor
public class UserDtoService implements IUserDtoService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private IProjectDtoService projectService;

    @NotNull
    @Autowired
    private ITaskDtoService taskService;

    @NotNull
    @Autowired
    public IUserDtoRepository repository;

    @NotNull
    @Override
    @Transactional
    public UserDTO create(@Nullable final String login, @Nullable final String password) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL.toString());
        repository.saveAndFlush(user);
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO create(@Nullable final String login, @Nullable final String password, @Nullable final String email) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExist(email)) throw new ExistsEmailException(login, email);
        @NotNull UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        user.setRole(Role.USUAL.toString());
        repository.saveAndFlush(user);
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @NotNull UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role.toString());
        repository.saveAndFlush(user);
        return user;
    }

    @Override
    public boolean isLoginExist(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) return false;
        return repository.existsByLogin(login);
    }

    @Override
    public boolean isEmailExist(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) return false;
        return repository.existsByEmail(email);
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        UserDTO users = repository.findByLogin(login);
        return users;
    }

    @Nullable
    @Override
    public UserDTO findByEmail(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) throw new LoginEmptyException();
        return repository.findByEmail(email);
    }

    @NotNull
    @Override
    public UserDTO findOneById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final UserDTO model = repository.findOneById(id);
        if (model == null) throw new UserNotFoundException();
        return model;
    }

    @Nullable
    @Override
    @Transactional
    public UserDTO removeOne(@Nullable final UserDTO model) throws Exception {
        if (model == null) return null;
        @Nullable final String userId = model.getId();
        repository.remove(model.getId());
        taskService.removeAll(userId);
        projectService.removeAll(userId);
        return model;
    }

    @Nullable
    @Override
    @Transactional
    public UserDTO removeByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) return null;
        return removeOne(user);
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO removeByEmail(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) throw new EmailEmptyException("login", email);
        @Nullable final UserDTO user = findByEmail(email);
        if (user == null) return null;
        return removeOne(user);
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO setPassword(@Nullable final String id, @Nullable final String password) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final UserDTO user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        repository.saveAndFlush(user);
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO updateUser(@Nullable final String id,
                              @Nullable final String firstName,
                              @Nullable final String lastName,
                              @Nullable final String middleName) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final UserDTO user = findOneById(id);
        user.setFirstName((firstName == null) ? "" : firstName);
        user.setLastName((lastName == null) ? "" : lastName);
        user.setMiddleName((middleName == null) ? "" : middleName);
        repository.saveAndFlush(user);
        return user;
    }

    @Override
    @Transactional
    public void lockUserByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        repository.saveAndFlush(user);
    }

    @Override
    @Transactional
    public void unlockUserByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        repository.saveAndFlush(user);
    }

    @NotNull
    @Override
    @Transactional
    public Collection<UserDTO> set(@NotNull Collection<UserDTO> models) throws Exception {
        if (models == null) throw new ProjectNotFoundException();
        clear();
        for (@NotNull final UserDTO user : models) {
            repository.saveAndFlush(user);
        }
        return models;
    }

    @Override
    @Transactional
    public void clear() {
        repository.deleteAll();
    }

    @NotNull
    @Override
    public List<UserDTO> findAll() {
        return repository.findAll();
    }

    @Override
    public long getSize() {
        return repository.count();
    }

}
