package ru.t1.chubarov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import javax.persistence.EntityManager;

public interface IConnectionService {

    @SneakyThrows
    @NotNull
    EntityManager getEntityManager();

}
