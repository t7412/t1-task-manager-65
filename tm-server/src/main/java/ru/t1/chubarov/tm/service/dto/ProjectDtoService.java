package ru.t1.chubarov.tm.service.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.chubarov.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.chubarov.tm.api.service.dto.IProjectDtoService;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.exception.entity.ModelNotFoundException;
import ru.t1.chubarov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.chubarov.tm.exception.field.*;
import ru.t1.chubarov.tm.dto.model.ProjectDTO;

import java.util.Collection;
import java.util.List;

@Service
@NoArgsConstructor
public class ProjectDtoService implements IProjectDtoService {

    @NotNull
    @Autowired
    public IProjectDtoRepository repository;

    @Override
    @Transactional
    public void create(@Nullable final String userId, @NotNull final String name) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name.isEmpty()) throw new NameEmptyException();
        @NotNull ProjectDTO project = new ProjectDTO(name, Status.NOT_STARTED.toString());
        project.setName(name);
        project.setUserId(userId);
        repository.saveAndFlush(project);
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO create(@Nullable final String userId, @NotNull final String name, @NotNull final String description) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull ProjectDTO project = new ProjectDTO(name, Status.NOT_STARTED.toString());
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        repository.saveAndFlush(project);
        return project;
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO add(@Nullable final String userId, @Nullable final ProjectDTO model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        repository.saveAndFlush(model);
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO updateById(@Nullable final String userId, @Nullable final String id, @NotNull final String name, @NotNull final String description) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name.isEmpty()) throw new NameEmptyException();
        @NotNull final ProjectDTO project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        repository.saveAndFlush(project);
        return project;
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO changeProjectStatusById(@Nullable final String userId, @Nullable final String id, @NotNull final Status status) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final ProjectDTO project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status.toString());
        repository.saveAndFlush(project);
        return project;
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) return false;
        return repository.countAllByUserIdAndId(userId, id) > 0;
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAllByUserId(userId);
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll() throws Exception {
        return repository.findAll();
    }

    @NotNull
    @Override
    public ProjectDTO findOneById(@NotNull String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final ProjectDTO model = repository.findFirstById(id);
        if (model == null) throw new ModelNotFoundException();
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO remove(@Nullable final String userId, @Nullable final ProjectDTO model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        repository.deleteById(model.getId());
        return model;
    }

    @Override
    @Transactional
    public void removeAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteByUserId(userId);
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO removeOneById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable ProjectDTO model = new ProjectDTO();
        repository.deleteByUserIdAndId(userId, id);
        return model;
    }

    @Override
    public long getSize() {
        return repository.count();
    }

    @Override
    public long getSize(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.countAllByUserId(userId);
    }

    @NotNull
    @Override
    @Transactional
    public Collection<ProjectDTO> add(@NotNull final Collection<ProjectDTO> models) throws Exception {
        if (models == null) throw new ProjectNotFoundException();
        for (@NotNull final ProjectDTO project : models) {
            repository.saveAndFlush(project);
        }
        return models;
    }

    @NotNull
    @Override
    @Transactional
    public Collection<ProjectDTO> set(@NotNull final Collection<ProjectDTO> models) throws Exception {
        if (models == null) throw new ProjectNotFoundException();
        repository.deleteAll();
        for (@NotNull final ProjectDTO project : models) {
            repository.saveAndFlush(project);
        }
        return models;
    }

    @Override
    @Transactional
    public void clear() {
        repository.deleteAll();
    }

}
