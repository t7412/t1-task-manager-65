package ru.t1.chubarov.tm.log;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class OperationEvent {

    private OperationType type;

    private Object entity;

    private String table;

    private long timestamp = System.currentTimeMillis();

    public OperationEvent(@Nullable final OperationType type, @Nullable final Object entity) {
        this.type = type;
        this.entity = entity;
    }

}
