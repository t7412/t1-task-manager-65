package ru.t1.chubarov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.t1.chubarov.tm.dto.model.SessionDTO;

import java.util.List;

@Repository
@Scope("prototype")
public interface ISessionDtoRepository extends IDtoRepository<SessionDTO> {

    @NotNull
    @Query("SELECT p FROM SessionDTO p ")
    List<SessionDTO> findAll();

    @Nullable
    List<SessionDTO> findAllByUserId(@Nullable String userId);

    @Nullable
    SessionDTO findFirstById(@NotNull String id);

    @Nullable
    SessionDTO findFirstByIdAndUserId(@Nullable String userId, @Nullable String id);

    void deleteAll();

    void deleteByUserId(@Nullable String userId);

    void deleteById(@Nullable String Id);

    void deleteByUserIdAndId(@Nullable String userId, @Nullable String id);

    long count();

    long countAllByUserId(@Nullable String userId);

    @NotNull
    Boolean existsByUserIdAndId(@Nullable String userId, @Nullable String id);

}
