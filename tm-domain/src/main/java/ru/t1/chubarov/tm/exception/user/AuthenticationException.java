package ru.t1.chubarov.tm.exception.user;

import ru.t1.chubarov.tm.exception.field.AbstractFieldException;

public final class AuthenticationException extends AbstractFieldException {

    public AuthenticationException() {
        super("Error. Incorrect login or password entered. Please try again.");
    }

}
